/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benjamas.ox_program;

import java.util.Scanner;

/**
 *
 * @author sany
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player("X");
        playerO = new Player("O");
        table = new Table(playerX, playerO);

    }

    public void startBoard() {
        table.startBoard();
    }

    public void board() {
        table.board();
    }

    public void input() {
        while (true) {
            System.out.println(table.getTurn().getName() + " turn");
            System.out.println("Please input Row Col : ");
            int row = kb.nextInt();
            int col = kb.nextInt();

            if (table.setRowCol(row, col)) {
                break;
            } else {
                System.out.println("");
                System.out.println("!! This position is NOT empty Please enter again !!");
                board();
                continue;
            }
        }
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void run() {
        startBoard();
        while (true) {
            input();
            board();
            table.switchPlayer();
            table.winnerCheck();
            if (table.isWhoWin() == true) {
                table.printLastWhoWin();
                System.out.println("Play again? [y/n]");
                char ask = kb.next().charAt(0);
                if (ask == 'y') {
                    newGame();
                    startBoard();
                } else {
                    break;
                }
            }

        }
        System.out.println("Thank you \n" + "Bye bye....");
        //table.printLastWhoWin();

    }

}
