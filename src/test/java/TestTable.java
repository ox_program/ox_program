/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.benjamas.ox_program.Player;
import com.benjamas.ox_program.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author sany
 */
public class TestTable {

    public TestTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testRow2ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testRow3ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testCol1ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testCol2ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testCol3ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testXXUp() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(3, 1);
        table.setRowCol(2, 2);
        table.setRowCol(1, 3);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testXXDown() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals("X", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    // ---------------------  O ------------------------- //
    public void testRow1ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testRow2ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testRow3ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testCol1ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testCol2ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testCol3ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testOXUp() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(3, 1);
        table.setRowCol(2, 2);
        table.setRowCol(1, 3);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }

    public void testOXDown() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals("O", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }
    
    // -----------  test fail  -------------- //
    
    public void testFail() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        assertEquals(false, table.isWhoWin());
    }
    
    public void testFail2() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(3, 1);
        table.setRowCol(2, 2);
        table.switchPlayer();
        table.setRowCol(1, 3);
        assertEquals(false, table.isWhoWin());
    }
    
    public void testDraw() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.switchPlayer();
        table.setRowCol(1, 2);
        table.switchPlayer();
        table.setRowCol(1, 3);
        table.switchPlayer();
        table.setRowCol(2, 2);
        table.switchPlayer();
        table.setRowCol(2, 1);
        table.switchPlayer();
        table.setRowCol(2, 3);
        table.switchPlayer();
        table.setRowCol(3, 2);
        table.switchPlayer();
        table.setRowCol(3, 1);
        table.switchPlayer();
        table.setRowCol(3, 3);
        table.switchPlayer();
        assertEquals("DRAW", table.winnerCheck());
        assertEquals(true, table.isWhoWin());
    }
}
